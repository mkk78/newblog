@extends('layouts.app')

@section('content')
	
	@include('admin.includes.errors')


	<div class="card">
		<div class="card-header">
			Edit new tag
		</div>

		<div class="card-body">
			<form action="{{ route('tags.update', ['id' => $tag->id]) }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group">
					<label for="name">Tag</label>
					<input type="text" value="{{$tag->tag}}" name="tag" class="form-control">
				</div>


				<div class="form-group">
					<div class="text-center">
						<button class="btn btn-success" type="submit">Edit Tag</button>
					</div>
				</div>

			</form>
		</div>
	</div>
@stop
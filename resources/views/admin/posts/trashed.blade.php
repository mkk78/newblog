@extends('layouts.app')

@section('content')

	<div class="card">

		<div class="card-header">
			Trashed Posts
		</div>


		<div class="card-body">
				<table class="table table-hover">
		<thead>
			<th>Image</th>
			<th>Title</th>
			<th>Editing</th>
			<th>Restore</th>
			<th>Destroy</th>
		</thead>
		<tbody>
			@if($posts->count() > 0)
				@foreach($posts as $post)
					<tr>
						<td>
							<img src="{{ $post->featured }}" alt="{{$post->title}}" width="100px">
						</td>

						<td>
							{{ $post->title}}
						</td>
						<td>
							Edit
						</td>

						<td>
							<a href="{{route('posts.restore', ['id' => $post->id])}}" class="btn btn-success btn-sm">Restore</a>
						</td>

						<td>
							<a href="{{route('posts.kill', ['id' => $post->id])}}" class="btn btn-danger btn-sm">Delete</a>
						</td>

					</tr>
				@endforeach
			@else
				<th colspan="5" class="text-center">No trashed posts</th>
			@endif
		</tbody>
	</table>
		</div>
	</div>

@stop